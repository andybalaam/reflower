use crate::images::{IMAGES, PIXELS_PER_SQUARE};
use crate::model::Model;
use crate::screen::Screen;
use crate::smolpxl_bindings::SmolpxlScreen;
use crate::{GAME_HEIGHT, GAME_WIDTH};

pub fn view_model(
    pixels: &mut [u8],
    width: usize,
    height: usize,
    smolpxl_screen: &SmolpxlScreen,
    _model: &Model,
) {
    let required_width = (GAME_WIDTH * PIXELS_PER_SQUARE) as usize;
    let required_height = (GAME_HEIGHT * PIXELS_PER_SQUARE) as usize;
    if required_width != width || required_height != height {
        smolpxl_screen.set_size(required_width, required_height);
        return;
    }

    let mut screen = Screen::new(pixels, width, height);
    screen.draw(10, 10, &IMAGES.test_image);
}
