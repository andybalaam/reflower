use std::collections::VecDeque;

use crate::direction::Direction;

pub struct Model {
    pub queued_input: VecDeque<Direction>,
}

impl Model {
    pub fn new() -> Model {
        Model {
            queued_input: VecDeque::new(),
        }
    }
}
