use js_sys::{JsString, Object, Reflect};
use wasm_bindgen::prelude::*;
use web_sys::console;

use crate::direction::Direction;
use crate::model::Model;
use crate::smolpxl_bindings::RunningGame;

const MAX_QUEUED_INPUT: usize = 3;

fn name_of_input(input: JsValue) -> Result<String, String> {
    let input = Object::try_from(&input);
    if let Some(input) = input {
        Reflect::get(input, &JsString::from("name"))
            .map(|s| s.as_string().unwrap())
            .map_err(|_| String::from("input did not have a name"))
    } else {
        Err(String::from("input was not an object"))
    }
}

fn read_input(running_game: &RunningGame, model: &mut Model) {
    for input in running_game.input().iter() {
        match name_of_input(input) {
            Err(e) => console::error_1(&e.into()),
            Ok(s) => match s.as_str() {
                "UP" => model.queued_input.push_front(Direction::Up),
                "DOWN" => model.queued_input.push_front(Direction::Down),
                "LEFT" => model.queued_input.push_front(Direction::Left),
                "RIGHT" => model.queued_input.push_front(Direction::Right),
                _ => (),
            },
        }
        // Forget older input if we've stored too much
        model.queued_input.truncate(MAX_QUEUED_INPUT);
    }
}

pub fn update_model(running_game: RunningGame, mut model: &mut Model) {}
