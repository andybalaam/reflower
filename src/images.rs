use once_cell::sync::Lazy;

pub static IMAGES: Lazy<Images> = Lazy::new(|| Images::new());

pub const PIXELS_PER_SQUARE: i32 = 17;

pub struct Image {
    pub width: u32,
    pub height: u32,
    pub bytes: Vec<u8>,
}

impl Image {
    fn from(img: image::DynamicImage) -> Image {
        if let image::DynamicImage::ImageRgba8(img) = img {
            Image {
                width: img.width(),
                height: img.height(),
                bytes: img.into_vec(),
            }
        } else {
            panic!("Unexpected pixel format. All images should RGBA 8-bit");
        }
    }
}

pub struct Images {
    pub test_image: Image,
}

impl Images {
    fn new() -> Images {
        Images {
            test_image: Image::from(decode(include_bytes!(
                "png/test_image.png"
            ))),
        }
    }
}

fn decode(bytes: &[u8]) -> image::DynamicImage {
    image::load_from_memory_with_format(bytes, image::ImageFormat::Png)
        .expect("Unable to decode as PNG.")
}
