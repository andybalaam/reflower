use wasm_bindgen::prelude::*;

#[wasm_bindgen(module = "smolpxl.js")]
extern "C" {
    pub type Game;

    #[wasm_bindgen(method, js_name = setTitle)]
    pub fn set_title(this: &Game, title: &str);

    #[wasm_bindgen(method, js_name = setFps)]
    pub fn set_fps(this: &Game, frames_per_second: usize);

    #[wasm_bindgen(method, js_name = sendPopularityStats)]
    pub fn send_popularity_stats(this: &Game);

    #[wasm_bindgen(method, js_name = showSmolpxlBar)]
    pub fn show_smolpxl_bar(this: &Game);

    #[wasm_bindgen(method, js_name = setSourceCodeUrl)]
    pub fn set_source_code_url(this: &Game, url: &str);

    #[wasm_bindgen(method, js_name = setSize)]
    pub fn set_size(this: &Game, width: i32, height: i32);

    #[wasm_bindgen(method, js_name = setBorderColor)]
    pub fn set_border_color(this: &Game, color: &[u8]);

    #[wasm_bindgen(method, js_name = setBackgroundColor)]
    pub fn set_background_color(this: &Game, color: &[u8]);

    #[wasm_bindgen(method, js_name = setTitleMessage)]
    pub fn set_title_message(this: &Game, msg: js_sys::Array);

    pub type RunningGame;

    #[wasm_bindgen(method)]
    pub fn input(this: &RunningGame) -> js_sys::Array;

    #[wasm_bindgen(method, js_name = receivedInput)]
    pub fn received_input(this: &RunningGame, name: &str) -> JsValue;

    #[wasm_bindgen(method, js_name = endGame)]
    pub fn end_game(this: &RunningGame);

    pub type SmolpxlScreen;

    #[wasm_bindgen(method, js_name = setSize)]
    pub fn set_size(this: &SmolpxlScreen, width: usize, height: usize);

    #[wasm_bindgen(method, js_name = messageTopLeft)]
    pub fn message_top_left(this: &SmolpxlScreen, msg: &str);

    #[wasm_bindgen(method, js_name = messageTopMiddle)]
    pub fn message_top_middle(this: &SmolpxlScreen, msg: &str);

    #[wasm_bindgen(method, js_name = messageTopRight)]
    pub fn message_top_right(this: &SmolpxlScreen, msg: &str);

    #[wasm_bindgen(method, js_name = messageBottomLeft)]
    pub fn message_bottom_left(this: &SmolpxlScreen, msg: &str);

    #[wasm_bindgen(method, js_name = messageBottomMiddle)]
    pub fn message_bottom_middle(this: &SmolpxlScreen, msg: &str);

    #[wasm_bindgen(method, js_name = messageBottomRight)]
    pub fn message_bottom_right(this: &SmolpxlScreen, msg: &str);

    #[wasm_bindgen(method)]
    pub fn dim(this: &SmolpxlScreen);

    #[wasm_bindgen(method)]
    pub fn message(this: &SmolpxlScreen, msg: js_sys::Array);
}
