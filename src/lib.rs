mod direction;
mod images;
mod model;
mod screen;
mod smolpxl_bindings;
mod update;
mod utils;
mod view;

use images::PIXELS_PER_SQUARE;
use wasm_bindgen::prelude::*;

use crate::model::Model;
use crate::smolpxl_bindings::{Game, RunningGame, SmolpxlScreen};
use crate::update::update_model;
use crate::utils::js_array;
use crate::view::view_model;

const GAME_HEIGHT: i32 = 12;
const GAME_WIDTH: i32 = 12;

#[wasm_bindgen]
pub struct ModelHolder {
    model: Model,
}

#[wasm_bindgen]
pub fn setup_game(game: Game) {
    utils::set_panic_hook();

    game.set_title("Reflower");
    //game.send_popularity_stats();
    game.set_fps(60);
    game.show_smolpxl_bar();
    game.set_source_code_url("https://codeberg.org/andybalaam/reflower");
    game.set_size(
        GAME_WIDTH * PIXELS_PER_SQUARE,
        GAME_HEIGHT * PIXELS_PER_SQUARE,
    );
    game.set_border_color(&[200, 200, 200]);
    game.set_background_color(&[0, 0, 40]);
    game.set_title_message(js_array(&[
        "Reflower",
        "",
        "Bring back the wildness.",
        "",
        "<SELECT> to start",
        "<UP>, <DOWN>, <LEFT>, <RIGHT>, <BUTTON1> to control",
        "<MENU> to pause",
    ]));
}

#[wasm_bindgen]
pub fn update(
    running_game: RunningGame,
    mut model_holder: ModelHolder,
) -> ModelHolder {
    update_model(running_game, &mut model_holder.model);
    model_holder
}

#[wasm_bindgen]
pub fn view(
    pixels: &mut [u8],
    width: usize,
    height: usize,
    smolpxl_screen: &SmolpxlScreen,
    model_holder: &ModelHolder,
) {
    view_model(pixels, width, height, smolpxl_screen, &model_holder.model);
}

#[wasm_bindgen]
pub fn new_model() -> ModelHolder {
    ModelHolder {
        model: Model::new(),
    }
}
