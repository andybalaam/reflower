SMOLPXL_JS_VERSION := v0.4.0
DOWNLOAD_URL := https://gitlab.com/smolpxl/smolpxl/-/raw

all: test

compile:
	cargo fmt
	wasm-pack build --target=web
	cp www/* pkg/

test: compile
	cargo test
	wasm-pack test --firefox --headless

run: compile
	cd pkg && python3 -m http.server

dist: test
	@echo Now upload the contents of pkg somewhere.

download-smolpxl-js:
	curl --silent --output www/smolpxl.js \
		"${DOWNLOAD_URL}/${SMOLPXL_JS_VERSION}/public/smolpxl.js"
	curl --silent --output www/smolpxl.css \
		"${DOWNLOAD_URL}/${SMOLPXL_JS_VERSION}/public/smolpxl.css"
	# Patch smolpxl.js to be a module
	echo "export default Smolpxl;" >> www/smolpxl.js
