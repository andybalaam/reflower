# Reflower

A multiplayer against-the-clock game of restoring wildness to barren landscapes.

It is written in Rust, compiled to WebAssembly.  It uses the
[Smolpxl library](https://gitlab.com/smolpxl/smolpxl) as a game framework,
and is published to the [Smolpxl games](https://smolpxl.artificialworlds.net/)
web site.

## Building

First, install Rust and wasm-pack as described in the
[Rust and WebAssembly book](https://rustwasm.github.io/docs/book/game-of-life/setup.html).
You should also make sure you have GNU Make.

Then, to compile and run tests:

```sh
make
```

## Running locally

To run a development server (requires Python3):

```sh
make run
```

## Packaging to upload

Run:

```sh
make dist
```

and then upload the contents of `pkg` to a web-accessible location.

## Valid dependency versions

The build should work with any recent Rust and wasm-pack,
but for reference, it has been tested with these versions:

```sh
rustc 1.70.0 (90c541806 2023-05-31)
wasm-pack 0.12.0
```

## Updating smolpxl.js

We store a copy of smolpxl.js in the source code.  To update it, modify
[Makefile](Makefile), changing the value of `SMOLPXL_JS_VERSION` to specify the
version you want, and run:

```sh
make download-smolpxl-js
```

## License and credits

Copyright 2023 Andy Balaam and contributors, released under the
[AGPLv3 license](LICENSE) or later.

Contains icons from the
[Feather Icons](https://github.com/feathericons/feather) set, which is
Copyright 2013-2017 Cole Bemis, and released under the
[MIT License](https://github.com/feathericons/feather/blob/8b5d6802fa8fd1eb3924b465ff718d2fa8d61efe/LICENSE).

Uses [shareon](https://shareon.js.org/) by Nikita Karamov to provide the
social sharing buttons.  (The code is dynamically loaded when the Share button
is clicked.)

## Code of conduct

Please note that this project is released with a
[Contributor Code of Conduct](code_of_conduct.md).  By participating in this
project you agree to abide by its terms.

[![Contributor Covenant](images/contributor-covenant-v2.0-adopted-ff69b4.svg)](code_of_conduct.md)
